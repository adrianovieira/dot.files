"set termencoding=utf8
""set paste
syntax on
set encoding=utf8
set fileencodings=utf-8
set cindent
set showmatch
set expandtab
set tabstop=4
set shiftwidth=4
set nocompatible
set number
set encoding=utf-8
set autoindent
set softtabstop=4
set ignorecase
imap <F6> <Insert>
nnoremap <F5> :setl noai nocin nosi inde=<CR>
inoremap <c-s> <c-o>:Update<CR><CR>

