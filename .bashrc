# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

alias l='ls -la' 

# User specific aliases and functions

export PS1='\n\[\e[0;34m\][\[\e[0;36m\] `date +"%d/%m/%Y %H:%M:%S"` \[\e[0;34m\]][ \[\e[0;32m\]\u\[\e[0m\]@\[\e[0;36m\]\h \[\e[0;34m\]][ \[\e[0;36m\]$PWD \[\e[0;34m\]]\n[\[\e[0;31m\]`git status 2>/dev/null|grep Changes|cut -d" " -f1` `echo "$( git branch 2>/dev/null |grep \* )" 2>/dev/null`\[\e[0;34m\]]\[\e[0m\] => '
